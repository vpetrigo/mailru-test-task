#include <iostream>
#include "downloader.hpp"

#include <unistd.h>

void display_usage() noexcept {
  std::cout << "Usage: Downloader <filename>" << std::endl;
  std::cout << "Arguments:" << std::endl;
  std::cout << "\t-h: display that help" << std::endl;
}

std::string get_url_from_cmd(int argc, char **argv) noexcept {
  char c;
  const std::string options{"h"};
  std::string url;

  while ((c = getopt(argc, argv, options.c_str())) != -1) {
    switch (c) {
      // handle incoming url
      case 'h':
      default:
        display_usage();
        break;
    }
  }

  if (optind < argc) {
    url = argv[optind];
  }
  return url;
}

int main(int argc, char *argv[]) {
  auto url = get_url_from_cmd(argc, argv);

  if (!url.empty()) {
    try {
      Downloader d{url};

      d.download();
    }
    catch (const std::exception &e) {
      std::cerr << "Error: " << e.what() << std::endl;
    }
  }

  return 0;
}
