#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <algorithm>
#include <cstring>
#include <fstream>
#include <iostream>
#include <iterator>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <arpa/inet.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include "http_parser.hpp"
#include "url_parser.hpp"

class Downloader {
 public:
  explicit Downloader(const std::string &s) : up{s} {}

  // prohibit copying
  Downloader(const Downloader &d) = delete;
  Downloader &operator=(const Downloader &d) = delete;

  void download() const {
    auto servinfo = get_connection_settings();

    if (!servinfo) {
      std::cerr << "Cannot get information about host server" << std::endl;
      std::cerr << "Exit..." << std::endl;

      return;
    }
    // TODO: should we care if servinfo contains several instance for IPv4 and
    // IPv6?
    try {
      auto client_socket = get_socket(servinfo);

      connect_to_server(client_socket, servinfo);
      send_request(client_socket);
      read_response(client_socket);
      close_connection(client_socket);
    }
    catch (const std::exception &e) {
      std::cerr << "Error: " << e.what() << std::endl;
    }
    // clean up a service info list
    freeaddrinfo(servinfo);
  }

 private:
  struct addrinfo *get_connection_settings() const {
    struct addrinfo hints = {0};
    struct addrinfo *servinfo = nullptr;
    int status;

    // use either IPv4 or IPv6
    hints.ai_family = AF_UNSPEC;
    // use TCP
    hints.ai_socktype = SOCK_STREAM;

    if ((status = getaddrinfo(up.host.c_str(), up.port.c_str(), &hints,
                              &servinfo)) != 0) {
      // error occured
      std::cerr << std::string{gai_strerror(status)} << std::endl;
    }

    return servinfo;
  }

  void close_connection(int socket_fd) const {
    int status = shutdown(socket_fd, SHUT_RDWR);

    if (status != 0) {
      throw std::runtime_error{strerror(errno)};
    }

    if ((status = close(socket_fd)) != 0) {
      throw std::runtime_error{strerror(errno)};
    }
  }

  void read_response(int socket_fd) const {
    int bytes_accept = 0;
    bool headers_get = false;
    constexpr std::size_t buf_len = 1024;
    std::string headers;
    char data[buf_len];
    auto data_begin = data;
    size_t data_read = 0;
    // counter for non-whitespace characters which may occure at the end of data
    unsigned short wsp_control_seq = 0;
    std::vector<char> data_chunk;

    // get headers and chunk of data
    while (!headers_get &&
           (bytes_accept = recv(socket_fd, data, buf_len, 0)) > 0) {
      char *delimiter_ptr = nullptr;
      auto data_end = data + bytes_accept;
      std::size_t data_offset = 0;
      // if we met '\r''\n' control sequences in the previous chunks of data
      if (check_first_characters(wsp_control_seq, data, bytes_accept, data_offset) ||
          look_for_http_delimiter(data_begin, data_end, headers, data_offset)) {
        // we found headers, so get the rest portion of data
        headers_get = true;
        data_read = bytes_accept - data_offset;
        data_chunk.resize(data_read);
        // calculate where data begins
        // get chunk of data after headers
        std::copy(data + data_offset, data_end, data_chunk.begin());
      }
      else {
        // if we did not find HTTP delimiter there might be 2 cases:
        //   1. we have character at the end of the received data
        //   2. we have '\r', '\r\n', '\r\n\r' at the end
        wsp_control_seq = check_last_three_characters(data_end, bytes_accept);
        headers.append(data);
      }
    }

    check_package_receive(bytes_accept);
    // parse headers
    HTTP_resp_parser rp{headers};

    if (rp.get_status() != "200") {
      std::cerr << "File: \"" << up.filename << "\" "
                << "cannot be downloaded" << std::endl;
      std::cerr << "Status: " << rp.get_status() << " " << rp.get_status_msg()
                << std::endl;

      return;
    }

    std::string filename = up.filename;

    if (filename.empty()) {
      filename = "index.html";
    }

    std::ofstream ofs{filename};

    // write chunk of data readed before
    auto wfile_iterator = std::ostreambuf_iterator<char>(ofs);
    std::copy(data_chunk.begin(), data_chunk.end(), wfile_iterator);

    auto fields = rp.get_fields();
    auto content_length_string = fields.find("Content-Length");

    if (content_length_string == fields.end()) {
      std::cerr << "Wrong response from server" << std::endl;
      return;
    }

    auto content_length =
        std::strtoul(content_length_string->second.c_str(), nullptr, 10);
    // read the rest of content
    while (data_read != content_length &&
           (bytes_accept = recv(socket_fd, data, buf_len, 0)) > 0) {
      data_read += bytes_accept;
      std::copy(data, data + bytes_accept, wfile_iterator);
    }

    check_package_receive(bytes_accept);
  }

  int get_socket(const struct addrinfo *servinfo) const {
    auto client_socket = socket(servinfo->ai_family, servinfo->ai_socktype,
                                servinfo->ai_protocol);

    if (client_socket == -1) {
      throw std::runtime_error{strerror(errno)};
    }

    return client_socket;
  }

  void connect_to_server(int socket_fd, struct addrinfo *servinfo) const {
    int status = connect(socket_fd, servinfo->ai_addr, servinfo->ai_addrlen);

    if (status != 0) {
      throw std::runtime_error{strerror(errno)};
    }
  }

  void send_request(int socket_fd) const {
    auto msg = form_get_request();
    auto bytes_sent = send(socket_fd, msg.c_str(), msg.size(), MSG_NOSIGNAL);

    if (bytes_sent == -1) {
      throw std::runtime_error{std::string{strerror(errno)}};
    }
  }

  std::string form_get_request() const noexcept {
    std::ostringstream oss;

    oss << "GET " << up.path << " HTTP/1.1" << std::endl;
    oss << "Host: " << up.host << std::endl << std::endl;

    return oss.str();
  }

  // function checks last three characters in the chunck of data which might be
  // control sequences
  unsigned short check_last_three_characters(const char *data_end,
                                             int bytes_accept) const {
    unsigned short wsp_control_seq = 0;

    if (bytes_accept > 3 &&
        !(std::isprint(*(data_end - 1)) || std::isblank(*(data_end - 1)))) {
      // there is a whitespace control sequence
      ++wsp_control_seq;
      // if we did not meet HTTP delimiter there might be up to 3 control
      // sequences
      std::reverse_iterator<const char *> check_back_seq(data_end - 1);
      std::reverse_iterator<const char *> sentinel(data_end - 3);

      while (check_back_seq != sentinel &&
             (*check_back_seq == '\r' || *check_back_seq == '\n')) {
        ++wsp_control_seq;
        ++check_back_seq;
      }

      return wsp_control_seq;
    }

    return 0;
  }

  void check_package_receive(int bytes_accepted) const {
    if (bytes_accepted == -1) {
      throw std::runtime_error{"Error during receive"};
    }
  }

  // function for checking first characters in the chunk of data whether they
  // may arrange HTTP delimiter or not
  bool check_first_characters(unsigned short wsp_control, char *data,
                              int bytes_accept,
                              std::size_t &data_offset) const {
    if (wsp_control) {
      auto chars_to_check = http_header_delim_size - wsp_control;

      // check whether we have necessary number of control sequences at the
      // beginning of data
      if (chars_to_check <= bytes_accept &&
          chars_to_check ==
              std::count_if(data, data + chars_to_check, [](char ch) {
                return (ch == '\r') || (ch == '\n');
              })) {
        data_offset = chars_to_check;

        return true;
      }
    }

    return false;
  }

  bool look_for_http_delimiter(char *begin, char *end,
                               std::string &headers,
                               std::size_t &data_offset) const {
    char *delimiter_ptr = nullptr;
    const std::string http_header_delim{"\r\n\r\n"};

    if ((delimiter_ptr = std::search(begin, end, http_header_delim.begin(),
                                     http_header_delim.end())) != end) {
      // get header size; + 2 for getting last field \r\n
      auto headers_size = std::distance(begin, delimiter_ptr) + 2;
      auto crlf_delim_size = 2;
      // append to the header string
      headers.append(begin, headers_size);
      data_offset = headers_size + crlf_delim_size;

      return true;
    }

    return false;
  }

  // Static constant members
  static constexpr std::size_t http_header_delim_size = 4;
  // Data members
  URL_parser up;
};

#endif  // DOWNLOADER_H
