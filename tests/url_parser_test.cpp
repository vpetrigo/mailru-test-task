#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "url_parser.hpp"
#include <string>

TEST_CASE("HTTP URL", "[http]") {
  SECTION("URL without port specification") {
    std::string url{"http://www.example.com/"};
    URL_parser up{url};
    
    REQUIRE(up.schema == "http");
    REQUIRE(up.host == "www.example.com");
    REQUIRE(up.path == "/");
    REQUIRE(up.port == "80");
    REQUIRE(up.filename.empty());
    REQUIRE(up.query.empty());
  }
  
  SECTION("URL with port specification") {
    std::string url{"http://www.example.com:9999/"};
    URL_parser up{url};
    
    REQUIRE(up.schema == "http");
    REQUIRE(up.host == "www.example.com");
    REQUIRE(up.path == "/");
    REQUIRE(up.port == "9999");
    REQUIRE(up.filename.empty());
    REQUIRE(up.query.empty());
  }
  
  SECTION("URL with port specification without last backslash") {
    std::string url{"http://www.example.com:9999"};
    URL_parser up{url};
    
    REQUIRE(up.schema == "http");
    REQUIRE(up.host == "www.example.com");
    REQUIRE(up.path == "/");
    REQUIRE(up.port == "9999");
    REQUIRE(up.filename.empty());
    REQUIRE(up.query.empty());
  }

  SECTION("URL with colon, but without port specification") {
    std::string url{"http://www.example.com:"};
    URL_parser up{url};

    REQUIRE(up.schema == "http");
    REQUIRE(up.host == "www.example.com");
    REQUIRE(up.path == "/");
    REQUIRE(up.port == "80");
    REQUIRE(up.filename.empty());
    REQUIRE(up.query.empty());
  }
  
  SECTION("URL without port specification with path") {
    std::string url{"http://www.example.com/test/path/"};
    URL_parser up{url};
    
    REQUIRE(up.schema == "http");
    REQUIRE(up.host == "www.example.com");
    REQUIRE(up.path == "/test/path/");
    REQUIRE(up.port == "80");
    REQUIRE(up.filename.empty());
    REQUIRE(up.query.empty());
  }
  
  SECTION("URL with port specification with path") {
    std::string url{"http://www.example.com:9999/test/path/"};
    URL_parser up{url};
    
    REQUIRE(up.schema == "http");
    REQUIRE(up.host == "www.example.com");
    REQUIRE(up.path == "/test/path/");
    REQUIRE(up.port == "9999");
    REQUIRE(up.filename.empty());
    REQUIRE(up.query.empty());
  }
  
    SECTION("URL without port specification with path with file") {
    std::string url{"http://www.example.com/test/path/file.txt"};
    URL_parser up{url};
    
    REQUIRE(up.schema == "http");
    REQUIRE(up.host == "www.example.com");
    REQUIRE(up.path == "/test/path/file.txt");
    REQUIRE(up.port == "80");
    REQUIRE(up.filename == "file.txt");
    REQUIRE(up.query.empty());
  }
  
  SECTION("URL with port specification with path with filename") {
    std::string url{"http://www.example.com:9999/test/path/file.txt"};
    URL_parser up{url};
    
    REQUIRE(up.schema == "http");
    REQUIRE(up.host == "www.example.com");
    REQUIRE(up.path == "/test/path/file.txt");
    REQUIRE(up.port == "9999");
    REQUIRE(up.filename == "file.txt");
    REQUIRE(up.query.empty());
  }
  
  SECTION("Wrong HTTP URL") {
    std::string url{"www.example.com:9999/test/path/file.txt"};
    
    REQUIRE_THROWS(URL_parser up{url});
  }
}

TEST_CASE("HTTPS URL", "[https]") {
  SECTION("URL without anything") {
    std::string url{"https://www.example.com"};
    URL_parser up{url};

    REQUIRE(up.schema == "https");
    REQUIRE(up.host == "www.example.com");
    REQUIRE(up.path == "/");
    REQUIRE(up.port == "443");
    REQUIRE(up.filename.empty());
    REQUIRE(up.query.empty());
  }
}

TEST_CASE("BAD URLs", "[bad cases]") {
  SECTION("URL without schema") {
    std::string url{"example.com"};
    REQUIRE_THROWS(URL_parser up{url});
  }
}
