#ifndef HTTP_PARSER_H
#define HTTP_PARSER_H

#include <cctype>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>

class HTTP_resp_parser {
 public:
  explicit HTTP_resp_parser(const std::string& headers) {
    std::stringstream ss{headers};
    std::string line;

    // get status line
    std::getline(ss, line);
    read_status_line(line);

    while (std::getline(ss, line)) {
      auto delim_pos = line.find(":");

      if (delim_pos == std::string::npos) {
        // TODO: handle wrong header
        std::cerr << "Wrong header" << std::endl;
        break;
      }

      // get response field name and skip the ':' delimiter
      std::string field = line.substr(0, delim_pos++);

      // skip whitespaces after delimiter
      while (isspace(line[delim_pos])) {
        ++delim_pos;
      }

      std::string value;
      // check whether we exceed line size, because it might be an empty header
      // [Name] :
      if (delim_pos < line.size()) {
        value = line.substr(delim_pos);
      }
      fields[field] = value;
    }
  }

  const std::unordered_map<std::string, std::string>& get_fields() const {
    return fields;
  }

  const std::string& get_status() const { return status; }

  const std::string& get_status_msg() const { return status_msg; }

 private:
  // function for obtaining HTTP status code from server response
  void read_status_line(const std::string& status_line) {
    // pass the protocol field
    auto pos = status_line.find(' ');
    auto status_end_pos = status_line.find(' ', pos + 1);

    status = status_line.substr(pos + 1, status_end_pos - pos - 1);
    pos = status_end_pos;

    // if server provides a status line description, get it
    if (pos != std::string::npos) {
      status_msg = status_line.substr(pos + 1);
    }
  }

  // data fields
  std::string status;
  std::string status_msg;
  std::unordered_map<std::string, std::string> fields;
};

#endif  // HTTP_PARSER_H
