#ifndef URL_PARSER_H
#define URL_PARSER_H

#include <cctype>
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <string>

struct URL_parser {
  explicit URL_parser(const std::string& url) {
    const std::string schema_delim{"://"};
    std::size_t pos = 0;
    std::size_t delim_pos = 0;

    if ((delim_pos = url.find(schema_delim, pos)) == std::string::npos) {
      throw std::runtime_error{"Bad URL"};
    }

    schema = url.substr(pos, delim_pos - pos);
    // skip schema delimiter
    pos = delim_pos + schema_delim.size();
    // look for ':' or '/' delimiter
    delim_pos = url.find_first_of(":/", pos);

    if (delim_pos == std::string::npos) {
      // url does not specify any port or query
      path = "/";
      host = url.substr(pos);
      port = determine_port();
    }
    else {
      host = url.substr(pos, delim_pos - pos);

      if (url[delim_pos] == ':') {
        // port specification
        // skip the delimiter
        pos = ++delim_pos;
        while (std::isdigit(url[delim_pos])) {
          ++delim_pos;
        }

        port = url.substr(pos, delim_pos - pos);
        if (port.empty()) {
          // schema://host: case
          port = determine_port();
        }
        path = url.substr(delim_pos);

        if (path.empty()) {
          path = "/";
        }
      } else {
        // query specification
        // should determine port according to a schema. If we use getaddrinfo
        // port might be equal to schema
        port = determine_port();
        path = url.substr(delim_pos);
      }
    }

    // exctract query from the path field
    extract_query();
    // exctract filename
    extract_filename();
  }

  std::string schema;
  std::string host;
  std::string port;
  std::string path;
  std::string filename;
  std::string query;

 private:
  // determine port according to a protocol
  std::string determine_port() {
    if (schema == "http") {
      return {"80"};
    }
    else if (schema == "https") {
      return {"443"};
    }

    return {};
  }

  // exctract a query from a path. It might be that after some parser's steps
  // path contains
  // /path/to/smth/file?a=2&b=3
  // it must be push to the appropriate query field
  void extract_query() {
    constexpr char query_delim = '?';
    auto delim_pos = path.find(query_delim);

    if (delim_pos == std::string::npos) {
      // no query in an input URL
      return;
    }

    path = path.substr(0, delim_pos);
    query = path.substr(delim_pos);
  }

  void extract_filename() {
    constexpr char filename_delim = '/';
    auto delim_pos = path.rfind(filename_delim);

    if (delim_pos == std::string::npos) {
      // no filename specified
      return;
    }

    // + 1 for skipping delimiter
    filename = path.substr(delim_pos + 1);
  }
};

#endif  // URL_PARSER_H
